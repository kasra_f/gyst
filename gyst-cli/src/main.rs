extern crate dirs;
extern crate failure;
extern crate serde;
extern crate serde_json;
extern crate termion;
extern crate gyst_lib;

use std::env;
use std::io::{stdin, stdout, Write};
use gyst_lib::todo;

fn main() -> Result<(), failure::Error> {
    let mut file = dirs::home_dir()
        .expect("Getting home directory with call to dirs::home_dir() failed.")
        .join(".gyst_todo");

    let mut todos = todo::Todos::load(&mut file)?;
    if handle_args(&mut todos) {
        todos.print();
        todos.save(&mut file)
    } else {
        Ok(())
    }
}

fn handle_args(todos: &mut todo::Todos) -> bool {
    let mut args = env::args();

    // Ignore program name
    args.next();

    match args.next() {
        None => {
            true
        }
        Some(option) => match option.as_ref() {
            "-a" => parse_add(&mut args, todos),
            "-d" => parse_del(&mut args, todos),
            _ => {
                println!("Argument not recognized: '{}'. Please use '--help' to see a list of valid arguments.", option);
                false
            }
        },
    }
}

fn parse_add(args: &mut env::Args, todos: &mut todo::Todos) -> bool {
    let mut note: Option<String> = None;
    let mut priority = todo::Priority::Medium;

    while let Some(arg) = args.next() {
        let arg = arg.as_ref();

        match arg {
            "Low"    => priority = todo::Priority::Low,
            "Medium" => priority = todo::Priority::Medium,
            "High"   => priority = todo::Priority::High,
            other => {
                if note.is_none() {
                    note = Some(other.to_owned());
                } else {
                    // @Todo better logging
                    println!(
                        "Warn: Todo argument not recognized: '{}'. Ignoring it.",
                        other
                    );
                }
            }
        }
    }

    match note {
        None => {
            println!("Error: Todo note not found. Doing nothing.");
            false
        }
        Some(note) => {
            todos.add_note(note, priority);
            true
        }
    }
}

fn parse_del(args: &mut env::Args, todos: &mut todo::Todos) -> bool {
    let mut done = false;

    let mut args = args.peekable();

    if args.peek().is_none() {
        println!("To delete a note, you must either pass the note id, or a pattern matching the note.");
        return false;
    }
    
    while let Some(arg) = args.next() {
        // Check if it's an ID
        let num = arg.parse::<u32>();
        if num.is_ok() {
            let num = num.unwrap();

            if todos.is_valid_id(num) {
                todos.del_note(num);
                done = true;
            } else {
                println!("Invalid note id: {}", num);
            }
        }

        if !done {
            // Look for a note starting with the passed value
            let hint = arg.to_lowercase();
            let mut marked: Vec<todo::TodoItem> = Vec::new();

            for note in &todos.items {
                let lowercase_note: String = note.note.to_lowercase();

                // @Todo This should be a fuzzy matcher.
                // @Todo Do fish-like auto-completion?

                if lowercase_note.contains(hint.as_ref() as &str) {
                    marked.push(note.clone());
                }
            }

            if marked.len() == 0 {
                println!("No note found matching: {}", arg);
            }
            else if marked.len() == 1 {
                todos.del_note(marked.first().unwrap().id);
                done = true;
            } else {
                // Failed to identify note. Print list of options.
                println!("Failed to identify note. Please enter an ID from the following:");

                for note in &marked {
                    println!("    {} -> \"{}\"", note.id, note.note);
                }

                done = false;
                let ids: Vec<u32> = marked.iter().map(|note| note.id).collect();

                while !done {
                    print!("Please select one: ");
                    stdout().flush();

                    let mut input = String::new();

                    stdin()
                        .read_line(&mut input)
                        .expect("Failed to read from stdin. This should not happen.");

                    match input.trim_end().parse::<u32>() {
                        Ok(id) => {
                            if ids.contains(&id) {
                                todos.del_note(id);
                                done = true;
                            } else {
                                print!("The given ID is not valid.")
                            }
                        }
                        Err(_) => print!("Input not recognized."),
                    }

                    if !done {
                        println!(" Please enter a valid ID from the list above.");
                    }
                }
            }
        }
    }

    done
}
