use failure;
use failure::format_err;
use serde::{Deserialize, Serialize};
use serde_json;
use std::cmp::min;
use std::error::Error;
use std::fs::File;
use std::io::{stdout, BufReader, BufWriter, Write};
use std::path::Path;
use termion::{clear, color, cursor, style};

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub enum Priority {
    High,
    Medium,
    Low,
}

#[derive(Debug, Serialize, Deserialize)]
// @Todo better name.
pub struct Todos {
    pub name: String,
    pub items: Vec<TodoItem>,
    curr_id: u32,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct TodoItem {
    pub id: u32,
    pub note: String,
    done: bool,
    priority: Priority,
}

impl Todos {
    pub fn load(path: &Path) -> Result<Self, failure::Error> {
        match serde_json::from_reader(BufReader::new(File::open(path)?)) {
            Ok(result) => Ok(result),
            Err(e) => Err(format_err!(
                "Deserializing failed. This should not happen: {}",
                e.description().to_owned()
            )),
        }
    }

    pub fn save(&self, path: &Path) -> Result<(), failure::Error> {
        let writer = BufWriter::new(File::create(path)?);
        match serde_json::to_writer(writer, self) {
            Ok(()) => Ok(()),
            Err(e) => Err(format_err!(
                "Serializing failed. This should not happen: {}",
                e.description().to_owned()
            )),
        }
    }

    pub fn print(&self) {
        // @Todo this needs some major cleanup.
        let term_size = termion::terminal_size().unwrap();

        let height = min(4 + self.items.len(), term_size.1 as usize - 2);
        let width = min(
            6 + self.items.iter().map(|x| x.note.len()).max().unwrap(),
            term_size.0 as usize,
        );

        let blue = color::Fg(color::Blue);
        let orange = color::Fg(color::Rgb(0xcb, 0x4b, 0x16));
        let red = color::Fg(color::Red);

        // Draw borders
        let mut buffer: Vec<Vec<char>> = Vec::with_capacity(height);

        for _ in 0..height {
            let mut row = Vec::with_capacity(width);

            for _ in 0..width {
                row.push(' ');
            }

            buffer.push(row);
        }

        buffer[0][1] = '+';
        buffer[0][width - 2] = '+';
        buffer[2][1] = '+';
        buffer[2][width - 2] = '+';
        buffer[height - 1][1] = '+';
        buffer[height - 1][width - 2] = '+';

        for x in 2..width - 2 {
            buffer[0][x] = '-';
            buffer[2][x] = '-';
            buffer[height - 1][x] = '-';
        }

        buffer[1][1] = '|';
        buffer[1][width - 2] = '|';

        for y in 3..height - 1 {
            buffer[y][1] = '|';
            buffer[y][width - 2] = '|';
        }

        println!("{}{}", clear::All, cursor::Goto(2, 1));

        for row in &buffer {
            let line: String = row.into_iter().collect();
            println!("{}", line);
        }

        // Borders have been printed. Print notes.

        // List name
        print!("{}{}{}", blue, style::Bold, cursor::Goto(4, 3));
        print!("{}", self.name);
        stdout().flush().unwrap();

        let mut med = Vec::new();
        let mut low = Vec::new();
        let mut row = 5;

        print!("{}{}", cursor::Goto(4, row), color::Fg(color::Red));

        for item in &self.items {
            if !item.done {
                match item.priority {
                    Priority::High => {
                        row += 1;
                        let line = if item.note.len() + 5 <= width {
                            item.note.clone()
                        } else {
                            format!("{}...", item.note[..width - 9].to_owned())
                        };
                        print!("{}{}", line, cursor::Goto(4, row));
                    }
                    Priority::Medium => med.push(item),
                    Priority::Low => low.push(item),
                }
            }
        }

        print!("{}", orange);

        for item in &med {
            row += 1;
            let line = if item.note.len() + 5 <= width {
                item.note.clone()
            } else {
                format!("{}...", item.note[..width - 9].to_owned())
            };
            print!("{}{}", line, cursor::Goto(4, row));
        }

        print!("{}", color::Fg(color::Green));

        for item in low {
            row += 1;
            let line = if item.note.len() + 5 <= width {
                item.note.clone()
            } else {
                format!("{}...", item.note[..width - 9].to_owned())
            };
            print!("{}{}", line, cursor::Goto(4, row));
        }

        print!("{}", style::Reset);
        println!();
    }

    pub fn add_note(&mut self, note: String, priority: Priority) {
        if note == "" {
            // @Todo How can I writer a proper CLI? Should I just println here?
            // Orange: cb4b16
            println!(
                "{}Warn: Tried to add empty note. Ignoring.{}",
                color::Fg(color::Rgb(0xcb, 0x4b, 0x16)),
                style::Reset
            );
        } else {
            self.curr_id += 1;
            self.items.push(TodoItem::new(self.curr_id, note, priority));
        }
    }

    pub fn del_note(&mut self, id: u32) {
        // @Todo this is dumb dumb dumb dumb dumb
        let mut index = Option::None;

        for (i, item) in self.items.iter().enumerate() {
            if item.id == id {
                index = Option::Some(i);
            }
        }

        match index {
            None => println!(
                "Failed to find todo item with id {}. This should not happen.",
                id
            ),
            Some(index) => {
                let id = self.items.remove(index).id;

                // Reuse if latest ID.
                // @Todo Could this be a security flaw?
                if id == self.curr_id {
                    self.curr_id -= 1;
                }
            }
        }
    }

    pub fn is_valid_id(&self, id: u32) -> bool {
        if id <= self.curr_id {
            for item in &self.items {
                if item.id == id {
                    return true;
                }
            }
        }

        false
    }
}

impl TodoItem {
    fn new(id: u32, note: String, priority: Priority) -> TodoItem {
        TodoItem {
            id,
            note,
            priority,
            done: false,
        }
    }
}

impl Clone for TodoItem {
    fn clone(&self) -> Self {
        TodoItem::new(self.id, self.note.clone(), self.priority.clone())
    }
}
